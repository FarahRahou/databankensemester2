USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
	DECLARE Teller INT;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'
    BEGIN
		SELECT 'State 45002 opgevangen. Geen probleem' ; 
    END;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
		SELECT 'Een algemene fout opgevangen.'; 
    END;
    
	SET Teller = FLOOR(RAND() * ANDER_GETAL) + 1;
    
	IF Teller = 1 THEN
        SIGNAL SQLSTATE '45001';
	ELSEIF Teller = 2 THEN
        SIGNAL SQLSTATE '45002';
	ELSE 
		SIGNAL SQLSTATE '45003';
    END IF;
    
END$$

DELIMITER ;