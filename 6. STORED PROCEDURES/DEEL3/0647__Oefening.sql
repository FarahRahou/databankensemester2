USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop`(IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT(0);
    loop_demo:  LOOP
        IF  counter < extraReleases THEN
            CALL MockAlbumReleaseWithSuccess(@succes);
			SET counter = counter +1;
        ELSE
            LEAVE  loop_demo;
        END  IF;
    END LOOP;
END$$

DELIMITER ;