USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT(0);
	DECLARE numberOfBands INT DEFAULT(0);
	DECLARE randomAlbumId INT DEFAULT(0);
	DECLARE randomBandId INT DEFAULT(0);
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SELECT 'Nieuwe releases konden niet worden toegevoegd';
    END;
    
    SELECT COUNT(*) FROM Albums INTO numberOfAlbums;
    SELECT COUNT(*) FROM Bands INTO numberOfBands;
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
	SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
	
    INSERT INTO AlbumReleases(Bands_Id, Albums_Id)
    VALUES (randomBandId, randomAlbumId);

END$$

DELIMITER ;