USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSucces`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleaseWithSucces` (OUT succes BOOL)
BEGIN
	DECLARE numberOfAlbums INT DEFAULT(0);
	DECLARE numberOfBands INT DEFAULT(0);
	DECLARE randomAlbumId INT DEFAULT(0);
	DECLARE randomBandId INT DEFAULT(0);
    DECLARE totalInAlbumReleases INT DEFAULT(SELECT COUNT(*) FROM AlbumReleases);
    
    SELECT COUNT(*) FROM Albums INTO numberOfAlbums;
    SELECT COUNT(*) FROM Bands INTO numberOfBands;
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
	SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;

	INSERT INTO AlbumReleases(Bands_Id, Albums_Id)
	VALUES (randomBandId, randomAlbumId);
		
	IF((SELECT COUNT(*) FROM AlbumReleases) > totalInAlbumReleases) THEN
		SET succes = 1;
	ELSE 
		SET succes = 0;
    END IF;
END$$

DELIMITER ;