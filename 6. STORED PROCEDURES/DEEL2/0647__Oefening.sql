USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT(0);
    adding_releases: LOOP
		IF counter < extraReleases THEN
			CALL MockAlbumReleaseWithSucces(@succes);
			SET counter = counter + 1;
		ELSE
			LEAVE adding_releases;
		END IF;
    END LOOP;
END$$

DELIMITER ;