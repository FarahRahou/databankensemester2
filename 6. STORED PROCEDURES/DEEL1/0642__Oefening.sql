USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(IN someDate DATE, OUT numberCleaned INT)
BEGIN

-- Deel 2 van de opdracht
SET numberCleaned = (SELECT COUNT(*) FROM Lidmaatschappen 
WHERE Einddatum IS NOT NULL AND Einddatum < someDate);

-- Deel 1 van de opdracht 
SET SQL_SAFE_UPDATES = 0;
DELETE FROM Lidmaatschappen
WHERE Einddatum IS NOT NULL AND Einddatum < someDate;
SET SQL_SAFE_UPDATES = 1;

END$$

DELIMITER ;