USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres` (OUT aantal TINYINT)
BEGIN
    SELECT COUNT(*)
    INTO aantal
    FROM genres;
END$$