USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(IN naamTekst VARCHAR(50))
BEGIN
    SELECT *
    FROM liedjes
    WHERE Titel LIKE CONCAT(naamTekst, '%');
END$$