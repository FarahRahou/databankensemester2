USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(IN newTitel VARCHAR(50), IN bands_Id INT)

BEGIN

START TRANSACTION;
INSERT INTO Albums (Titel) VALUES (newTitel);
INSERT INTO AlbumReleases (Bands_Id, Albums_Id) VALUES (bands_Id, last_insert_id());
COMMIT; 

END$$

DELIMITER ;
