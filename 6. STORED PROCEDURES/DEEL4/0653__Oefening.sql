USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=root@localhost PROCEDURE `GetAlbumDuration2` (IN numberAlbum INT, OUT totalDuration SMALLINT UNSIGNED)
SQL SECURITY INVOKER
BEGIN
DECLARE ok INT DEFAULT 0;
-- Nodige variabelen 
DECLARE songDuration TINYINT UNSIGNED;
-- CURSOR-GEDEELTE --
DECLARE albumLus CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_id = numberAlbum;
-- Controle, we gebruiken continue omdat we wel willen nakijken 
-- maar niet stoppen. Eens gecontroleert of er nog een waarde is gaan we voort.
DECLARE CONTINUE HANDLER FOR NOT FOUND SET ok = 1;
    
SET totalDuration = 0;
-- We openen de cursor
OPEN albumLus;
-- We starten een loop (label gegeven)
getAlbum: LOOP
-- Fetch gedeelde
    FETCH albumLus INTO songDuration;
    IF ok = 1
	THEN
        LEAVE getAlbum;
	END IF;
SET totalDuration = totalDuration + songDuration;
-- Eindig de loop
END LOOP getAlbum;
-- Cursor afsluiten
CLOSE albumLus;
END$$

DELIMITER ;

CALL GetAlbumDuration(1, @totalDuration);
SELECT @totalDuration;