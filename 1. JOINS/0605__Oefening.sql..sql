Use modernways;
insert into Uitleningen ( startdatum, eindatum, boeken_id, leden_id) -- data in je databank toevoegen
values 
('2019-02-01','2019-02-15',1,3),		-- Altijd eerst het jaartal, dan de maand, dan de dag
('2019-02-16','2019-03-02',1,2),		-- Eerst het boek (1) en dan de persoon (2)
('2019-02-16','2019-03-02',5,2),
('2019-05-01',NULL, 5,1); 				-- verplicht 

-- Rechter muisklik op boeken en personen om te weten hoeveen boeken of personen er zijn
-- klik op select rows -Limit 1000 en dan uitvoeren 


-- Opdracht: 

-- Max heeft Norwegian Wood geleend van 1 februari 2019 tot 15 februari 2019.
-- Bavo heeft Norwegian Wood geleend van 16 februari 2019 tot 2 maart 2019.
-- Bavo heeft Pet Sematary geleend van 16 februari 2019 tot 2 maart 2019.
-- Yannick heeft Pet Sematary geleend van 1 mei 2019 en heeft het boek nog niet teruggebracht.