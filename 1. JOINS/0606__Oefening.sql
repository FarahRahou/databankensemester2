USE ModernWays;

SELECT boeken.Titel, leden.Naam
FROM uitleningen 											-- tabel  
     INNER JOIN leden ON uitleningen.leden_id = leden.Id	-- kolom, tabel.kolom, kolom 
     INNER JOIN boeken ON uitleningen.boeken_id = boeken.Id	-- kolom, tabel.kolom, kolom 


-- Opdracht:

-- Gebruik nu INNER JOIN met de tabel Personen, 
-- tabel Boeken en de tabel die deze twee entiteiten koppelt om weer te geven welke personen ooit een boek hebben uitgeleend (en welk boek dat was). 
-- Je toont alleen de naam van de persoon (als eerste kolom) en de titel van het boek (als tweede kolom).
