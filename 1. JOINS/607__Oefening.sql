USE Modernways;
SELECT Omschrijving
FROM Taken
LEFT JOIN Leden
ON Taken.Leden_Id = Leden.Id
WHERE Taken.Leden_Id IS NULL 

-- Er is een tabel Taken en een tabel Leden. Bij taken staat (door middel van een verwijzing) welk lid een bepaalde taak uitvoert 
-- (zoals eerder in de cursus "Databanken Intro"). Toon nu alle taken die niet aan iemand zijn toegewezen