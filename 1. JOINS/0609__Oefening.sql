select games.Titel, platformen.Naam
from games
left join releases on games.Id = releases.Games_Id
left join platformen on platformen.Id = releases.Platformen_Id

-- Toon alle titels van games met hun bijbehorend platform, als er een is. 
-- Toon ook games waarvoor het platform niet meer ondersteund wordt (d.w.z. waarvoor geen info in Releases staat).
--  Gebruik hiervoor een samenstelling van twee JOINs.