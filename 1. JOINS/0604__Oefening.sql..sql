use modernways; 
Create table uitleningen (		-- Schrijf zelf een tabel, Uitleningen
eindatum date, 					-- Een uitlening heeft een startdatum en eventueel een einddatum. 
startdatum date, 				-- Deze datums stel je voor met het DATE-datatype.
boeken_id int not null,			-- not null= niet verplicht 
leden_id int not null,			-- boeken_id en leden_id, daar gebruiken we een int voor omdat het nummers zijn. 

constraint fk_uitleningen_boeken foreign key (boeken_id) --  leden koppelt aan boeken die ze hebben uitgeleend in de bibliotheek. 
references boeken (Id),
leden_id int not null ); 


