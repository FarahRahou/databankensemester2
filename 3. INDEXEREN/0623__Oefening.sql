-- toont alle geldige combinaties van liedjestitels en genres
-- alle rock liedjes oplijsten 

select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id
where Naam = 'Rock';

-- stap 1: index toevoegen: create index NaamIdx on Genres(Naam);