-- toont per naam het aantal keer dat iemand met die naam lid is van een groep
-- iemand die lid is van twee groepen, wordt dus twee keer geteld
-- naamgenoten zijn mogelijk en worden samen geteld

select Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
from Muzikanten inner join Lidmaatschappen
on Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
group by Familienaam, Voornaam
order by Voornaam, Familienaam;

-- STAP 1: Plan en timing bekijken 
-- STAP 2: Dit uitvoeren: drop index VoornaamFamilienaamIdx on Muzikanten;
-- STAP 3: Index toeveoegen : create index FamilienaamVoornaamIdx on Muzikanten(Familienaam,Voornaam);
-- van full tabel plan naar full index plan--> verbetering
