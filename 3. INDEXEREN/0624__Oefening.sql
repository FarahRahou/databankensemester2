-- toont combinaties van liedjes en bands
-- doet dit enkel voor liedjestitels die beginnen met 'A'
-- gaat van kort naar lang

SELECT Titel, Naam, Lengte FROM Liedjes
inner join Bands
on Liedjes.Bands_Id = Bands.Id
where Titel like 'A%'
order by Lengte;

-- STAP 1: code uitvoeren en timing bekijken bij de eerste lijn
-- van Timing (as measured by the server) 
-- STAP 2: INDEX maken : create index TitelIdx on Liedjes(Titel);
-- STAP 3: Weer de Timing checken (zou minder moeten zijn)
-- Execution plan ging van rood naar oranje 