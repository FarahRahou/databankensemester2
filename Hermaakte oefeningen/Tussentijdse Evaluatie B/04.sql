USE TussentijdseEvaluatie1adb;

select Gebruikers.Gebruikersnaam from Gebruikers
inner join Aankopen on Gebruikers_Id = Gebruikers.Id
group by Gebruikers.Gebruikersnaam
having count(*) >=10;
