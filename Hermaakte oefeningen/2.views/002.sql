use modernways;
CREATE VIEW auteursboeken
AS
SELECT boeken.titel, personen.voornaam
from publicaties
inner join personen on personen.id= personen_id
inner join boeken on boeken.id= boeken_id


-- Maak een view aan met naam AuteursBoeken waarmee je makkelijk een overzicht kan vragen van welke auteur welk(e) boek(en) heeft geschreven.