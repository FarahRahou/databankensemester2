use modernways; 

insert into boekennaarauteurs (boeken_id, auteurs_id)
values

(1,10),
(2,10),
(3,16),
(4,16),
(5,17),
(6,18),
(6,16),
(7,17),
(7,19);

-- Haruki Murakami schreef Norwegian Wood en Kafka on the Shore
-- Neil Gaiman schreef American Gods en The Ocean at the End of the Lane
-- Stephen King schreef Pet Sematary
-- Terry Pratchett en Neil Gaiman schreven samen Good Omens
-- Stephen King en Peter Straub schreven samen The Talisman