use modernways; 
select games.titel, platformen.naam
from games left join platformen
on games.id= platformen.id

UNION ALL

select titel, naam
from games right join platformen
on games.id= platformen.id

-- Toon alle titels van games met hun bijbehorend platform, als er een is. Toon ook games waarvoor het platform niet meer ondersteund wordt
-- (d.w.z. waarvoor geen info in Releases staat). Gebruik hiervoor een samenstelling van twee JOINs.