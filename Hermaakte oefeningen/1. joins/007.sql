use modernways; 
SELECT boeken.Titel, leden.voornaam
FROM uitleningen
     INNER JOIN boeken ON uitleningen.boeken_Id = boeken.Id
     INNER JOIN leden ON uitleningen.leden_Id = leden.Id

-- Gebruik nu INNER JOIN met de tabel Personen, de tabel Boeken en 
-- de tabel die deze twee entiteiten koppelt om weer te geven welke personen ooit een boek hebben uitgeleend (en welk boek dat was).
-- Je toont alleen de naam van de persoon (als eerste kolom) en de titel van het boek (als tweede kolom).