use modernways; 
select omschrijving
from taken left join leden
on taken.leden_id = leden.id 
where leden.id is null 


-- Er is een tabel Taken en een tabel Leden. Bij taken staat (door middel van een verwijzing) 
-- welk lid een bepaalde taak uitvoert (zoals eerder in de cursus "Databanken Intro"). Toon nu alle taken die niet aan iemand zijn toegewezen.