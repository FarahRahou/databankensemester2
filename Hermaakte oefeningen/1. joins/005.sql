use modernways;
create table uitlening(
startdatum date,
eindatum date,
boeken_id int not null,
leden_id int not null,

CONSTRAINT fk_uitlening_leden FOREIGN KEY (leden_Id) REFERENCES leden(Id),
CONSTRAINT fk_uitlening_boeken FOREIGN KEY (boeken_Id) REFERENCES boeken(Id)
);


-- Schrijf zelf een tabel, Uitleningen, die leden koppelt aan boeken die ze hebben uitgeleend in de bibliotheek. 
-- Een uitlening heeft een startdatum en eventueel een einddatum. Deze datums stel je voor met het DATE-datatype.