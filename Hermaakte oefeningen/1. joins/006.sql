use modernways;
insert into uitlening(startdatum, eindatum, leden_id, boeken_id)
values 
('2019-02-01','2019-02-15',3,1),
('2019-02-16','2019-03-02',2,1),
('2019-02-16','2019-03-02',2,5),
('2019-05-01', null,1,5);


-- Max heeft Norwegian Wood geleend van 1 februari 2019 tot 15 februari 2019.
-- Bavo heeft Norwegian Wood geleend van 16 februari 2019 tot 2 maart 2019.
-- Bavo heeft Pet Sematary geleend van 16 februari 2019 tot 2 maart 2019.
-- Yannick heeft Pet Sematary geleend van 1 mei 2019 en heeft het boek nog niet teruggebracht.