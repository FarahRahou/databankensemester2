use modernways;  
select voornaam, omschrijving
from taken left join leden
on taken.leden_id = leden.id 

-- Toon alle taken, met het lid dat de taak uitvoert. Als de taak door niemand wordt uitgevoerd, staat er een NULL. 
-- Doe dit met een soort JOIN die je nog niet hebt gebruikt, dus geen CROSS JOIN of INNER JOIN. 