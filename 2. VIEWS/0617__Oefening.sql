use modernways; 
create view AuteursBoekenRatings as
select AuteursBoeken.voornaam,
	   AuteursBoeken.titel,
	   gemiddelderatings.rating
from auteursboeken inner join gemiddelderatings 
on auteursboeken.Boeken_Id = gemiddelderatings.Boeken_Id;


-- Oefening
-- Maak de view AuteursBoekenRatings aan door een nieuwe view te maken gebaseerd op AuteursBoeken en GemiddeldeRatings

