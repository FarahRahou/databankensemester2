use modernways; 
ALTER VIEW AuteursBoeken 
AS
SELECT boeken.titel,
	   personen.Voornaam, 
       boeken.id as boeken_id
       
from publicaties
inner join boeken on boeken.id = boeken_id 
inner join personen on personen.id= personen_id

-- Oefening 615
-- Gebruik een ALTER VIEW om je bestaande view AuteursBoeken 
-- te voorzien van het Id uit de tabel Boeken. Toon Id hier wel als Boeken_Id.