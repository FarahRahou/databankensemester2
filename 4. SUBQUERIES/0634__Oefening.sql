USE modernways;
SELECT DISTINCT AVG(cijfer) AS "gemiddelde" FROM evaluaties
GROUP BY studenten_id HAVING (AVG(cijfer) <= ALL (SELECT AVG(cijfer) FROM evaluaties GROUP BY studenten_id));