USE ModernWays;

-- Een nieuwe tabel met de auteursgegevens Voornaam en Familienaam aanmaken
DROP TABLE IF EXISTS Personen;
CREATE TABLE Personen (
    Voornaam VARCHAR(255) NOT NULL,
    Familienaam VARCHAR(255) NOT NULL
);

-- Vullen met de juiste data door een subquery
INSERT INTO Personen(Voornaam, Familienaam)
SELECT DISTINCT Voornaam, Familienaam FROM Boeken;
    
-- Nakijken of er geen dubbelen meer zijn in de nieuwe tabel Personen
SELECT Voornaam, Familienaam FROM Personen
ORDER BY Voornaam, Familienaam;

-- De andere kolommen toevoegen aan tabel Personen
ALTER TABLE Personen ADD (
   Id INT AUTO_INCREMENT PRIMARY KEY,
   AanspreekTitel VARCHAR(30) NULL,
   Straat VARCHAR(80) NULL,
   Huisnummer VARCHAR (5) NULL,
   Stad VARCHAR (50) NULL,
   Commentaar VARCHAR (100) NULL,
   Biografie VARCHAR(400) NULL);
   
-- Nakijken of we hebben wat we willen
SELECT * FROM Personen ORDER BY Familienaam, Voornaam;
   
-- Kolom aanmaken voor de foreign key in tabel Boeken (om relatie te leggen tussen Personen en Boeken)
-- momenteel NULL, pas nadat we het hebben ingevuld voegen we de NOT NULL constraint toe
ALTER TABLE Boeken ADD Personen_Id INT NULL;

-- Foreign key kolom invullen 
SET SQL_SAFE_UPDATES = 0;
UPDATE Boeken CROSS JOIN Personen
SET Boeken.Personen_Id = Personen.Id
WHERE Boeken.Voornaam = Personen.Voornaam
AND Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- NOT NULL constraint toevoegen aan de kolom Personen_Id in tabel Boeken
ALTER TABLE Boeken CHANGE Personen_Id Personen_Id INT NOT NULL;

-- Nakijken of we hebben wat we willen
SELECT Voornaam, Familienaam, Personen_Id FROM Boeken;

-- Nu nog de dubbele kolommen verwijderen uit tabel Boeken
ALTER TABLE Boeken DROP COLUMN Voornaam,
DROP COLUMN Familienaam;

-- Foreign key constraint toevoegen
ALTER TABLE Boeken ADD CONSTRAINT fk_Boeken_Personen
FOREIGN KEY(Personen_Id) REFERENCES Personen(Id);

