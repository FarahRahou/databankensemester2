USE ModernWays;

-- Nieuwe auteur inserten
INSERT INTO Personen (
   Voornaam, Familienaam, AanspreekTitel
)
VALUES (
   'Hilary', 'Mantel', 'Mevrouw'
);

-- Een boek van de nieuwe auteur inserten
INSERT INTO Boeken (
   Titel, Stad, Uitgeverij, Verschijningsdatum,
   Herdruk, Commentaar, Categorie, Personen_Id
)
VALUES (
   'Wolf Hall', '', 'Fourth Estate; First Picador Edition First Printing edition',
   '2010', '', 'Goed boek', 'Thriller', 11
);